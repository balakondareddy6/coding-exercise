package com.gamesys.userregistration.dao;
/**
* Service to offer registration of the user & checking already user existing in the system.
* @author Annapu Bala
*
*/

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.gamesys.userregistration.dto.UserRegistrationDTO;

public class UserRegistrationDaoImpl {
	
   /**
	* In memory maintenance for user registration using static block.
	*
	*/
    private static Logger log = Logger.getLogger(UserRegistrationDaoImpl.class.getName());
	static List<UserRegistrationDTO>  listUserRegistrationDto = null;
    static {
    	log.info(" enter into UserRegistrationDao : static block : add new user information");
    	listUserRegistrationDto = new ArrayList<UserRegistrationDTO>();
    }
    
   /**
    * register the user in the system.
    *
    * @param username
    * @param password
    * @param dob 
    * @param ssn 
    * @return true if the user register in the system
    */
	public static boolean registration(String username, String password, String dob, String ssn){
		log.info(" enter into UserRegistrationDao : registration method : start");
		UserRegistrationDTO userRegistrationDto = new UserRegistrationDTO();
		userRegistrationDto.setUsername(username);
		userRegistrationDto.setPassword(password);
		userRegistrationDto.setDob(dob);
		userRegistrationDto.setSsn(ssn);
		listUserRegistrationDto.add(userRegistrationDto);
		log.info(" enter into UserRegistrationDao : registration method : end");
		return true;
	}
	
	   /**
	    * Checking the user already existing in the system.
	    * @param dob 
	    * @param ssn 
	    * @return true if the user already existing in the system
	    */
		
	public static boolean checkUserRegistration(String dob, String ssn ){
		boolean userAlreadyExist = false;
		log.info(" enter into UserRegistrationDao : checkUserRegistration method : start");
		if(listUserRegistrationDto != null && listUserRegistrationDto.size() != 0){
			for(UserRegistrationDTO user : listUserRegistrationDto){
				if(user.getDob().equals(dob) && user.getSsn().equals(ssn)){
					userAlreadyExist = true;
					break;
				}
			}
		}
		return userAlreadyExist;
	}
}
