package com.gamesys.userregistration.service;

/**
* Service to offer registration of the user.
* @author Annapu Bala
*
*/
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;

import com.gamesys.userregistration.dao.UserRegistrationDaoImpl;
import com.gamesys.userregistration.validation.UserRegistrationValidation;
import javax.ws.rs.core.Response;
import com.gamesys.userregistration.dto.UserRegistrationDTO;

@Path("/registrationService")
public class RegistrationService {

	 Logger log = Logger.getLogger(RegistrationService.class.getName());
	 
	/**
	 * Register the user below with his/her personal data.
	 *
	 * @param username
	 * @param password
	 * @param dob the user's date of birth in ISO 8601 format
	 * @param ssn the user's social security number (United States)
	 * @return String User registration successful response if user could be registered, for any failure throws 
	 * an appropriate remote exception/ error
	 */
	    @POST
		@Path("/registration")
		@Consumes(MediaType.APPLICATION_JSON)
		public Response registration(UserRegistrationDTO userRegistrationDTO) throws Exception{

			String username = userRegistrationDTO.getUsername();
			String password = userRegistrationDTO.getPassword();
			String dob = userRegistrationDTO.getDob();
			String ssn = userRegistrationDTO.getSsn();
			log.debug("RegistrationService parameters : username" + username +" password "
			+password+" dob "+dob+ " ssn "+ssn);
			 
			String errorMessage = UserRegistrationValidation.validate(username, password, dob, ssn);
			 if(errorMessage == null){
				boolean userAlreadyExist = UserRegistrationDaoImpl.checkUserRegistration(dob, ssn);
				if(userAlreadyExist == false ){
				   UserRegistrationDaoImpl.registration(username, password, dob, ssn);
				   log.info("User registration successful");
				   return Response.status(201).entity("User registration successful").build(); 
			   }else{
				   log.info("User already exist in the system");
				   return Response.status(201).entity("User already exist in the system").build(); 
			    }
			 }else{
				 log.fatal(errorMessage);
				 return Response.status(201).entity(errorMessage).build();
			 }
			
	    }
}
