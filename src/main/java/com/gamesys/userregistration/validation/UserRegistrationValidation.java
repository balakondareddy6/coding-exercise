package com.gamesys.userregistration.validation;
/**
* Service to offer validation of a user against a 'blacklist' && DOB & SSN & UserName & Password. Blacklisted users
fail the validation.
* @author Annapu Bala
*
*/
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.gamesys.userregistration.dto.UserRegistrationDTO;


public class UserRegistrationValidation {

	private static Logger log = Logger.getLogger(UserRegistrationValidation.class.getName());
	
	/**
	* In memory maintenance for black list users using static block.
	*
	*/
    static List<UserRegistrationDTO>  blacklistUsers = null;
    static {
    	log.info(" enter into UserRegistrationValidation : static block : blacklistuser information");
    	blacklistUsers = new ArrayList<UserRegistrationDTO>();
    	UserRegistrationDTO blacklistuser = new UserRegistrationDTO();
    	blacklistuser.setSsn("123-45-6789");
    	blacklistuser.setDob("1980-12-24");
    	blacklistUsers.add(blacklistuser);
    	blacklistuser = new UserRegistrationDTO();
    	blacklistuser.setSsn("856-45-6789");
    	blacklistuser.setDob("1981-11-12");
    	blacklistUsers.add(blacklistuser);
    }
	
    /**
    * Validates a user against a black list using his date of birth and social
    security number as identifier and validates the username and password
    *
    * @param username - alphanumerical, no spaces
    * @param password - at least four characters, at least one upper case character, at least one number
    * @return true if the user could be validated and is not blacklisted, false if
    the user is blacklisted and therefore failed validation
    */
	public static String validate(String username, String password, String dob, String ssn){
		log.info("enter into UserRegistrationValidation : validate method : start");
		
		boolean validationflag = true;
		StringBuilder errormessages = new StringBuilder();
		String usernamereg= "^[a-zA-Z0-9]*$";
		if(username == null || (!username.matches(usernamereg))){
			errormessages.append("UserName should have AlphaNumerical characters and No Spaces ");
			validationflag = false;
		}
		String passwordreg = "^(?=.*[a-z])(?=.*[A-Z]).+$";
		if(password == null || password.length() < 4 || (!password.matches(passwordreg))){
			errormessages.append("Password should have atleast 4 characters, at least one upper case character, at least one number");
			validationflag = false;
		}
		
		for(UserRegistrationDTO user : blacklistUsers){
			if(user.getSsn().equals(ssn) && user.getDob().equals(dob)){
				errormessages.append(" You are not authorized person. Please contact administrator ");
				validationflag = false;
				break;
			}
		}
		
		log.info("enter into UserRegistrationValidation : validate method : end");
		if(validationflag == false){
			return errormessages.toString();
		}else{
			return null;
		}
			
    }
}
