package com.gamesys.userregistration.test;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


public class UserRegistrationTester {

	   private static Client client;
	   private static final String REST_SERVICE_URL = "http://localhost:8080/coding-exercise/rest/registrationService/registration";
	   private static WebResource webResource;
	   /*private static final String*/
	  
	  @BeforeClass 
	  public static void setUp() {
		 client = Client.create();
         webResource  = client.resource(REST_SERVICE_URL);
     }
	  
	  @Test
	  public void userRegistration(){
		  try {

				String input = "{\"username\":\"annapu\",\"password\":\"Welc\",\"dob\":\"1983-12-23\",\"ssn\":\"120-45-6789\"}";

				ClientResponse response = webResource.type("application/json")
				   .post(ClientResponse.class, input);

				if (response.getStatus() != 201) {
					throw new RuntimeException("Failed : HTTP error code : "
					     + response.getStatus());
				}
				//System.out.println(response.getEntity(String.class));
				Assert.assertEquals("User registration successful",response.getEntity(String.class));
				
			  } catch (Exception e) {

				e.printStackTrace();

			  }

	  }
	   
	  @Test
	  public void blcklistuser(){
		  try {

				String input = "{\"username\":\"annapu\",\"password\":\"Welc\",\"dob\":\"1981-11-12\",\"ssn\":\"856-45-6789\"}";

				ClientResponse response = webResource.type("application/json")
				   .post(ClientResponse.class, input);

				if (response.getStatus() != 201) {
					throw new RuntimeException("Failed : HTTP error code : "
					     + response.getStatus());
				}
				System.out.println(response.getEntity(String.class));
				Assert.assertEquals(" You are not authorized person. Please contact administrator ",response.getEntity(String.class));
				
			  } catch (Exception e) {

				e.printStackTrace();

			  }

	  }

	  @Test
	  public void regUserPwdValidation(){
		  try {

				String input = "{\"username\":\"annapu@\",\"password\":\"welc\",\"dob\":\"1981-11-12\",\"ssn\":\"856-45-6789\"}";

				ClientResponse response = webResource.type("application/json")
				   .post(ClientResponse.class, input);

				if (response.getStatus() != 201) {
					throw new RuntimeException("Failed : HTTP error code : "
					     + response.getStatus());
				}

				System.out.println("Output from Server .... \n");
				String output = response.getEntity(String.class);
				System.out.println(output);

			  } catch (Exception e) {

				e.printStackTrace();

			  }

	  }

		
}
